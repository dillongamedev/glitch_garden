﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour {

    public Texture2D fadeOutTexture; // the texture that will overlay the screen
    public float fadeSpeed = 0.8f; // the fading speed

    private int drawDepth = -1000; // the texture's order in the draw hierarchy: a low number means it renders on top
    private float alpha = 1.0f; // the texture's alpha will be between 1 and 0
    private int fadeDir = -1; // the direction to fade in = -1 or out -1

    // Use this for initialization
    private void OnGUI()
    {
        // fade out/in the alpha value using a direction, a speed and Time.deltaTime to convert the operation to seconds
        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        // force (clamp) the number between 0 and 1 because GUI.color uses alpha values between 0 and 1
        alpha = Mathf.Clamp01(alpha);

        // set color of our GUI (in this case our texture), All color values remain the same & the alpha is set to the alpha variable
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
    }

    public float BeginFade(int direction)
    {
        fadeDir = direction;
        return (fadeSpeed); // return the fadeSpeed so it's easy to time the Application.LoadLevel();
    }

    private void OnLevelWasLoaded(int level)
    {
        // alpha -1;
        BeginFade(-1);
    }
}
